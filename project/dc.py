import pygame, Buttons,os,sys,random,subprocess,tkMessageBox,socket          
from pygame.locals import *
from array import *
from Tkinter import *

from threading import Thread
window = Tk()
window.wm_withdraw()
progname = sys.argv[0]
progdir = os.path.dirname(progname)
sys.path.append(os.path.join(progdir,'gamelib'))
from popup_menu import PopupMenu
FPS=20
FPSCLOCK = pygame.time.Clock() 
pygame.init()
myfont = pygame.font.SysFont("monospace", 15)
k=0 
turn=0 #index of whose turn
b=0 #y cordinate of mouse
img_posx=0
img_posy=0
cardch=0 # index of card
WON=0 #won=1 detective won=2 mrx
reachablepoints=[int]*50
startpoint=(13,26,29,34,50,53,91,94,103,112,117,132,138,141,155,174,197,198)
did=0                                                 
start=0
displayvariable=0
with open('SCOTPOS.TXT') as f:  #contains info of cordinates
    position =[[int(x) for x in line.split()]for line in f]


with open('SCOTMAP.TXT') as f:    #contains info of where u can go using which ticket
    w, h = [int(x) for x in f.readline().split()]
    point =[[x if x.isalpha() else int(x) for x in line.split()]for line in f]



def searchcordi(point):
     
       i=0
       while point!=position[i][0]:
         i+=1
       return position[i]


def connectwithserver():
     global s,WON,turn,did,start,d1,d2,d3,d4,d5,x1,mapImg,displayvariable
  
     while True:
      if(turn%6!=did) and start==1:
          if turn%6==1 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective1.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective1.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective1.U-=1
                                        d1=searchcordi(received[1])
          elif turn%6==2 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective2.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective2.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective2.U-=1
                                        d2=searchcordi(received[1])

          elif turn%6==3 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective3.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective3.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective3.U-=1
                                        d3=searchcordi(received[1])
          elif turn%6==4 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective4.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective4.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective4.U-=1
                                        d4=searchcordi(received[1])
          elif turn%6==5 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective5.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective5.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective5.U-=1
                                        d5=searchcordi(received[1])
          elif turn%6==0:
                                        string=s.recv(1024)
                                        print string
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        cardch=received[0]
                                        if cardch=='B':
                                         mrx.B-=1
                                        elif cardch=='T':
                                         mrx.T-=1
                                        elif cardch=='U':
                                         mrx.U-=1
                                        elif cardch=='K':
                                         mrx.T-=1
                                        elif cardch=='X':
                                         mrx.U-=1
                                        x1=searchcordi(received[1])
                                        #turn+=1
                                        #print turn
                            
          #if turn%6!=0 :
                                        #caught(x1[0],received[1]) 
                                        #if WON==1:
                                          #break
          #mapImg=pygame.image.load('map.jpg')
          #mapImg=pygame.transform.scale(mapImg, (2036, 1618))
          #display()
          turn+=1
          print turn
          displayvariable=1

          #display()
     

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)         
host ='172.24.33.132'                                    
port = 1234                                                 
s.connect((host, port))
did=int(s.recv(1024))
#print did
#print "going to thread"
Thread(target=connectwithserver).start()
#print "made thread"


#print "waiting for string"
string=s.recv(4096)
print string
startpoint =[int(x) for x in string.split(" ")]
d1=startpoint[0]
d2=startpoint[1]
d3=startpoint[2]
d4=startpoint[3]
d5=startpoint[4]
x1=startpoint[5]

start=1

count=0
i=1
while count<=5:#
 if d1==position[i][0]:
  d1=position[i]
  count+=1
  i+=1
 elif d2==position[i][0]:
  d2=position[i]
  count+=1
  i+=1
 elif d3==position[i][0]:
  d3=position[i]
  count+=1
  i+=1
 elif d4==position[i][0]:
  d4=position[i]
  count+=1
  i+=1
 elif d5==position[i][0]:
  d5=position[i]
  count+=1
  i+=1
 elif x1==position[i][0]:
  x1=position[i]
  count+=1
  i+=1
 else:
  i+=1


def caught(a,b):
 global WON

 if a==b:
  WON=1
 

def handle_menu(e):
     # print 'Menu event: %s %s' % (e.name,e.text)
    if(e.text!='NOT_POSSIBLE'):
       return int(e.text)
    else:
       tkMessageBox.showinfo(title="ERROR", message="Please Select a Possible Move")
       return 0



class exit(object):
    #global window
    def __init__(self): 
        self.window = Tk()
        self.window.wm_title("Quit Game")
        self.label = Label (self.window, text= "Do you really want to Exit?")
        self.label.pack()

        Button(self.window, text="YES", command=self.yes,padx=5).pack()
       
        Button(self.window, text="NO", command=self.no).pack()
        self.window.mainloop()
         

    def yes(self):
       # print "yes"
        pygame.quit()
        self.window.destroy()
    
    def no(self):
      self.window.destroy()

class detective:
   movable=1 # 0=not able to move
   global d1,d2,d3,d4,d5,x1,b,char
   T=10
   B=8  
   U=4


   def __init__(self):
      self.b=b
      self.main()
        

   def card(self,current):
    global cardch
    if 0<b<60:
     cardch='B'
     return 'B'
    elif 60<b<120:
     cardch='T'
     return 'T'
    elif 120<b<180:
     cardch='U'
     return 'U'

  

   #reachable POINTS             
   def reachablepoint(self,char,current): 
    i=0                              
    global k,point
    k=1
    if char!='K':
     while point[i][0]<current:
       if point[i][1]==current:
         if point[i][2]==char:
           reachablepoints[k]=point[i][0]
           k=k+1
       i+=1
     if current!=199:
      while  i<=472  and point[i][0]==current:
       if point[i][2]==char:
         reachablepoints[k]=point[i][1]
         k+=1
       i+=1
    else:
     while point[i][0]<current:
      if point[i][1]==current:
       reachablepoints[k]=point[i][0]
       k+=1
      i+=1
     if current!=199:
      while point[i][0]==current:
       reachablepoints[k]=point[i][1]
       k+=1
       i+=1

    while k<2:
     reachablepoints[k]='NOT_POSSIBLE'
     k+=1
   

    menu=()
    for j in range(0,k):
      menu=menu+tuple((str(reachablepoints[j])).split(" "),)
    (a,b)=pygame.mouse.get_pos()
    if 0<b<180 and 1218<a<1364:
      PopupMenu(menu)




   def main(self):
        global turn
        
        if turn<=(22*6):
            if did%6==0 and mrx.movable==1:
              mrx()
            elif did%6==1 and detective1.movable==1:
              
               detective1()
            elif did%6==2 and detective2.movable==1:
             
               detective2()
            elif did%6==3 and detective3.movable==1:
              
               detective3()
            elif did%6==4 and detective4.movable==1:
              
              detective4()
            elif did%6==5 and detective5.movable==1:
              
               detective5()
            else:
               turn+=1
                   
            

        else:
          WON=2

class detective1(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d1[0]
     char=self.card(current)
     if getattr(detective1,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective1,char))
        self.reachablepoint(char,current)
        display()
    
class detective2(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d2[0]
     char=self.card(current)
     if getattr(detective2,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective2,char))
        self.reachablepoint(char,current)
        display()
     
class detective3(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()
   

  def main(self):
     current=d3[0]
     char=self.card(current)
     if getattr(detective3,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective3,char))
        self.reachablepoint(char,current)
        display()
    
class detective4(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d4[0]
     char=self.card(current)
     if getattr(detective4,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective4,char))
        self.reachablepoint(char,current)
        display()

class detective5(detective):
  def __init__(self):
    #super(detective1, self).__init__(self)
    detective.__init__(self)
    self.main()

  def main(self):
     current=d5[0]
     char=self.card(current)
     if getattr(detective5,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective5,char))
        self.reachablepoint(char,current)
        display()



class mrx(detective):
  #T=4
  B=3 
  U=3
  X=2
  K=5
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=x1[0]
     char=self.card(current)
     if getattr(mrx,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(mrx,char))
        self.reachablepoint(char,current) 
        display()
       

class Buttonclass:
  def __init__(self):
        self.Button1 = Buttons.Button()
        self.Button2 = Buttons.Button()
        self.Button3 = Buttons.Button()
        self.Button4 = Buttons.Button()
        self.Button5 = Buttons.Button()
        self.Button6 = Buttons.Button()
        self.Button7 = Buttons.Button()
        self.Button8 = Buttons.Button()
        self.Button9 = Buttons.Button()
        self.Button10 = Buttons.Button()
        self.Button11 = Buttons.Button()
        self.Button12 = Buttons.Button()
        self.update_display()
     
  def update_display(self):
        #Parameters:       s        surface,      color,       x,   y,   length, height, width,    text,      text_color
        self.Button1.create_button(DISPLAYSURF, (255,255,255), 1040, 650, 97,    55,    0,       "PLAY", (0,0,0))
        self.Button2.create_button(DISPLAYSURF, (255,255,255), 1150, 650, 97,    55,    0,       "MOVES", (0,0,0))
        self.Button3.create_button(DISPLAYSURF, (255,255,255), 1260, 650, 97,    55,    0,       "BACK", (0,0,0))
        self.Button4.create_button(DISPLAYSURF, (255,255,255), 1040, 582, 97,    55,    0,       "CHAT", (0,0,0))
        self.Button5.create_button(DISPLAYSURF, (255,255,255), 1150, 582, 97,    55,    0,       "FLAG", (0,0,0))
        self.Button6.create_button(DISPLAYSURF, (255,255,255), 1260, 582, 97,    55,    0,       "SOUND", (0,0,0))
        self.Button7.create_button(DISPLAYSURF, (255,255,255), 1040, 187, 41,    35,    0,       "Mr.X", (0,0,0))
        self.Button8.create_button(DISPLAYSURF, (255,255,255), 1095, 187, 41,    35,    0,       "Dt.1", (0,0,0))
        self.Button9.create_button(DISPLAYSURF, (255,255,255), 1150, 187, 41,    35,    0,       "Dt.2", (0,0,0))
        self.Button10.create_button(DISPLAYSURF, (255,255,255), 1205, 187, 41,    35,    0,       "Dt.3", (0,0,0))
        self.Button11.create_button(DISPLAYSURF, (255,255,255), 1260, 187, 41,    35,    0,       "Dt.4", (0,0,0))
        self.Button12.create_button(DISPLAYSURF, (255,255,255), 1315, 187, 43,    35,    0,       "Dt.5", (0,0,0))
        #pygame.display.flip()
    
  #def main(self):
        

   
                    
                     
if True: 
        
        
        def display(): 
          if mapImg.get_locked():
                mapImg.unlock()
          DISPLAYSURF.blit(mapImg, (0, 0),pygame.Rect(img_posx,img_posy,1033,710))
          DISPLAYSURF.blit(taxi, (1218, 60))
         # pygame.draw.rect(taxi, (190,190,190), (1200,50,146,60), 30)  
          DISPLAYSURF.blit(bus, (1218, 0))
          DISPLAYSURF.blit(underground, (1218, 120))
          mapImg.blit(img0, (2*x1[1]-20,2*x1[2]-40))
          mapImg.blit(img1, (2*d1[1]-20,2*d1[2]-40))
          mapImg.blit(img2, (2*d2[1]-20,2*d2[2]-40))
          mapImg.blit(img3, (2*d3[1]-20,2*d3[2]-40))
          mapImg.blit(img4, (2*d4[1]-20,2*d4[2]-40))
          mapImg.blit(img5, (2*d5[1]-20,2*d5[2]-40))
          label = myfont.render("Some text!", 1, (255,255,0))
          DISPLAYSURF.blit(label, (1033, 180))
          button=Buttonclass()
          button.update_display()
          pygame.display.update()
 
        def scroll(mousex,mousey):
          global img_posx
          global img_posy
          if 1033>mousex>850 and img_posx<990:
             img_posx+=20
          elif mousex<120 and img_posx>0:
             img_posx-=20
          elif mousey>660 and img_posy<900 and mousex<1033:
             img_posy+=20
          elif mousey<120 and img_posy>0 and mousex<1033:
             img_posy-=20
          #display()######
       
      
        DISPLAYSURF = pygame.display.set_mode((1364,710 ))
        mapImg=pygame.image.load('map.jpg')
        mapImg=pygame.transform.scale(mapImg, (2036, 1618))
        #DISPLAYSURF.blit(mapImg, (0, 0),pygame.Rect(img_posx,img_posy,1033,710))
        pygame.display.set_caption('detective %s'%did)
        img0=pygame.image.load('flag0.gif')
        img0=pygame.transform.scale(img0, (50, 50))
        img1=pygame.image.load('flag1.gif')
        img1=pygame.transform.scale(img1, (50, 50))
        #mapImg.blit(pawnImg1, (900,0))
        img2=pygame.image.load('flag2.gif')
        img2=pygame.transform.scale(img2, (50, 50))
        img3=pygame.image.load('flag3.gif')
        img3=pygame.transform.scale(img3, (50, 50))
        img4=pygame.image.load('flag4.gif')
        img4=pygame.transform.scale(img4, (50, 50))
        img5=pygame.image.load('flag5.gif')
        img5=pygame.transform.scale(img5, (50, 50))
        bus=pygame.image.load('bus.png')
        bus=pygame.transform.scale(bus, (146, 60))
        DISPLAYSURF.blit(bus, (1218, 0))
        taxi=pygame.image.load('taxi.png')
        taxi=pygame.transform.scale(taxi, (146, 60))
        DISPLAYSURF.blit(taxi, (1218, 60))
        underground=pygame.image.load('underground.png')
        underground=pygame.transform.scale(underground, (146, 60))
        DISPLAYSURF.blit(underground, (1218, 120))
        badge=pygame.image.load('dbadge.jpg')
        badge=pygame.transform.scale(badge, (200, 180))
        DISPLAYSURF.blit(badge, (1033, 0), (0,0, 185, 180))
        Buttonclass()
        pygame.display.update()
        display()
        
        while WON==0:
         if displayvariable==1:
          mapImg=pygame.image.load('map.jpg')
          mapImg=pygame.transform.scale(mapImg, (2036, 1618))
          display()
          displayvariable=0
         if detective1.movable==0 and detective2.movable==0 and detective3.movable==0 and detective4.movable==0 and detective5.movable==0:
          WON=2
          break
         '''if(turn%6!=did):
          if turn%6==1 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective1.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective1.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective1.U-=1
                                        d1=searchcordi(received[1])
          elif turn%6==2 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective2.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective2.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective2.U-=1
                                        d2=searchcordi(received[1])
          elif turn%6==3 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective3.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective3.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective3.U-=1
                                        d3=searchcordi(received[1])
          elif turn%6==4 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective4.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective4.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective4.U-=1
                                        d4=searchcordi(received[1])
          elif turn%6==5 :
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective5.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective5.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective5.U-=1
                                        d5=searchcordi(received[1])
          elif turn%6==0:
                                        string=s.recv(1024)
                                        received =[x if x.isalpha() else int(x) for x in string.split()]
                                        charch=received[0]
                                        if cardch=='B':
                                         mrx.B-=1
                                        elif cardch=='T':
                                         mrx.T-=1
                                        elif cardch=='U':
                                         mrx.U-=1
                                        elif cardch=='K':
                                         mrx.T-=1
                                        elif cardch=='X':
                                         mrx.U-=1
                                        d5=searchcordi(received[1])
                            
          if turn%6!=0 :
                                        caught(x1[0],received[1]) 
                                        if WON==1:
                                          break
         mapImg=pygame.image.load('map.jpg')
         mapImg=pygame.transform.scale(mapImg, (2036, 1618))
         display()
         turn+=1'''
       
            
         
         if detective1.movable==0 and detective2.movable==0 and detective3.movable==0 and detective4.movable==0 and detective5.movable==0:
          WON=2
         for event in pygame.event.get():
                (a,b)=pygame.mouse.get_pos()
                scroll(a,b)
                if event.type == pygame.QUIT:
                   pygame.quit()

                elif event.type == USEREVENT:
                    if event.code == 'MENU':
                        
                        if turn<=(22*6) and handle_menu(event)!=0 and turn%6==did:
                                       if turn%6==1 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective1.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective1.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective1.U-=1
                                        s.sendall((cardch+' '+str(handle_menu(event))))
                                        s.recv(1024)
                                        d1=searchcordi(handle_menu(event))
                                       elif turn%6==2 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective2.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective2.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective2.U-=1
                                        s.sendall((cardch+' '+str(handle_menu(event))))
                                        s.recv(1024)
                                        d2=searchcordi(handle_menu(event))
                                       elif turn%6==3 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective3.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective3.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective3.U-=1
                                        s.sendall((cardch+' '+str(handle_menu(event))))
                                        s.recv(1024)
                                        d3=searchcordi(handle_menu(event))
                                       elif turn%6==4 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective4.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective4.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective4.U-=1
                                        s.sendall((cardch+' '+str(handle_menu(event))))
                                        s.recv(1024)
                                        d4=searchcordi(handle_menu(event))
                                       elif turn%6==5 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective5.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective5.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective5.U-=1
                                        s.sendall((cardch+' '+str(handle_menu(event))))
                                        s.recv(1024)
                                        d5=searchcordi(handle_menu(event))
                                       if turn%6!=0 :
                                        caught(x1[0],handle_menu(event)) 

                                    
                                       mapImg=pygame.image.load('map.jpg')
                                       mapImg=pygame.transform.scale(mapImg, (2036, 1618))
                                       display()
                                       turn+=1


                elif event.type == MOUSEBUTTONDOWN:
                   (a,b)=pygame.mouse.get_pos()
                   if 1033<a<1364 and (187<b<222 or  582<b<710) :
                     buttonclass=Buttonclass()
                     if buttonclass.Button1.pressed(pygame.mouse.get_pos()):
                              play=1

                     elif buttonclass.Button2.pressed(pygame.mouse.get_pos()):
                              print "moves"

                     elif buttonclass.Button3.pressed(pygame.mouse.get_pos()):
                           print "exit()" 
                     elif buttonclass.Button4.pressed(pygame.mouse.get_pos()):
                            print "chat"
 
                     elif buttonclass.Button5.pressed(pygame.mouse.get_pos()):
                            print "flag"

                     elif buttonclass.Button6.pressed(pygame.mouse.get_pos()):
                              print "sound"

                     elif buttonclass.Button7.pressed(pygame.mouse.get_pos()):
                          #print "mrx"
                          if x1[1]>280:
                            img_posx=min(2*x1[1]-560,990)
                          elif x1[1]<280:
                            img_posx=0
                          if x1[2]>190:
                            img_posy=min(2*x1[2]-380,900)
                          elif x1[2]<190:
                            img_posy=0
                          display()
                     elif buttonclass.Button8.pressed(pygame.mouse.get_pos()):
                          if d1[1]>280:
                            img_posx=min(2*d1[1]-560,990)
                          elif d1[1]<280:
                            img_posx=0
                          if d1[2]>190:
                            img_posy=min(2*d1[2]-380,900)
                          elif d1[2]<190:
                            img_posy=0
                          display()
                     elif buttonclass.Button9.pressed(pygame.mouse.get_pos()):
                          if d2[1]>280:
                            img_posx=min(2*d2[1]-560,990)
                          elif d2[1]<280:
                            img_posx=0
                          if d2[2]>190:
                            img_posy=min(2*d2[2]-380,900)
                          elif d2[2]<190:
                            img_posy=0
                          display()
                     elif buttonclass.Button10.pressed(pygame.mouse.get_pos()):
                          if d3[1]>280:
                            img_posx=min(2*d3[1]-560,990)
                          elif d3[1]<280:
                            img_posx=0
                          if d3[2]>190:
                            img_posy=min(2*d3[2]-380,900)
                          elif d3[2]<190:
                            img_posy=0
                          display()
                     elif buttonclass.Button11.pressed(pygame.mouse.get_pos()):
                          if d4[1]>280:
                            img_posx=min(2*d4[1]-560,990)
                          elif d4[1]<280:
                            img_posx=0
                          if d4[2]>190:
                            img_posy=min(2*d4[2]-380,900)
                          elif d4[2]<190:
                            img_posy=0
                          display()
                     elif buttonclass.Button12.pressed(pygame.mouse.get_pos()):
                          if d5[1]>280:
                            img_posx=min(2*d5[1]-560,990)
                          elif d5[1]<280:
                            img_posx=0
                          if d5[2]>190:

                            img_posy=min(2*d5[2]-380,900)
                          elif d5[2]<190:
                            img_posy=0
                          display()
                     
                     buttonclass.update_display()  
                   #print did,turn
                   elif 0<b<180 and 1218<a<1364 and did==turn:
                      print did,turn
                      detective()
                      print "pressed"
                      
                 
                display()
                FPSCLOCK.tick(FPS)
                


        while WON==1: #detective won
           dwon=pygame.image.load('dwon.jpg')
           #dwon=pygame.transform.scale(dwon, (146, 60))
           DISPLAYSURF.blit(dwon, (0,0))
           pygame.display.update()
         

        while WON==2: # xwon
             xwon=pygame.image.load('xwon.jpg')
             #xwon=pygame.transform.scale(underground, (146, 60))
