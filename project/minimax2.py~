from sys import maxsize

import sys
import os
import unittest
class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

class PriorityQueue:
    def __init__(self):
        self.heapArray = [(0,0)]
        self.currentSize = 0

    def buildHeap(self,alist):
        self.currentSize = len(alist)
        self.heapArray = [(0,0)]
        for i in alist:
            self.heapArray.append(i)
        i = len(alist) // 2            
        while (i > 0):
            self.percDown(i)
            i = i - 1
                        
    def percDown(self,i):
        while (i * 2) <= self.currentSize:
            mc = self.minChild(i)
            if self.heapArray[i][0] > self.heapArray[mc][0]:
                tmp = self.heapArray[i]
                self.heapArray[i] = self.heapArray[mc]
                self.heapArray[mc] = tmp
            i = mc
                
    def minChild(self,i):
        if i*2 > self.currentSize:
            return -1
        else:
            if i*2 + 1 > self.currentSize:
                return i*2
            else:
                if self.heapArray[i*2][0] < self.heapArray[i*2+1][0]:
                    return i*2
                else:
                    return i*2+1

    def percUp(self,i):
        while i // 2 > 0:
            if self.heapArray[i][0] < self.heapArray[i//2][0]:
               tmp = self.heapArray[i//2]
               self.heapArray[i//2] = self.heapArray[i]
               self.heapArray[i] = tmp
            i = i//2
 
    def add(self,k):
        self.heapArray.append(k)
        self.currentSize = self.currentSize + 1
        self.percUp(self.currentSize)

    def delMin(self):
        retval = self.heapArray[1][1]
        self.heapArray[1] = self.heapArray[self.currentSize]
        self.currentSize = self.currentSize - 1
        self.heapArray.pop()
        self.percDown(1)
        return retval
        
    def isEmpty(self):
        if self.currentSize == 0:
            return True
        else:
            return False

    def decreaseKey(self,val,amt):
        # this is a little wierd, but we need to find the heap thing to decrease by
        # looking at its value
        done = False
        i = 1
        myKey = 0
        while not done and i <= self.currentSize:
            if self.heapArray[i][1] == val:
                done = True
                myKey = i
            else:
                i = i + 1
        if myKey > 0:
            self.heapArray[myKey] = (amt,self.heapArray[myKey][1])
            self.percUp(myKey)
            
    def __contains__(self,vtx):
        for pair in self.heapArray:
            if pair[1] == vtx:
                return True
        return False
        
class TestBinHeap(unittest.TestCase):
    def setUp(self):
        self.theHeap = PriorityQueue()
        self.theHeap.add((2,'x'))
        self.theHeap.add((3,'y'))
        self.theHeap.add((5,'z'))
        self.theHeap.add((6,'a'))
        self.theHeap.add((4,'d'))


    def testInsert(self):
        assert self.theHeap.currentSize == 5

    def testDelmin(self):
        assert self.theHeap.delMin() == 'x'
        assert self.theHeap.delMin() == 'y'
    
    def testDecKey(self):
        self.theHeap.decreaseKey('d',1)
        assert self.theHeap.delMin() == 'd'
        
#if __name__ == '__main__':
 #   unittest.main()

class Vertex:
    def __init__(self,num):
        self.id = num
        self.connectedTo = {}
        self.color = 'white'
        self.dist = sys.maxsize
        self.pred = None
        self.disc = 0
        self.fin = 0

    # def __lt__(self,o):
    #     return self.id < o.id
    
    def addNeighbor(self,nbr,weight=0):
        self.connectedTo[nbr] = weight
        
    def setColor(self,color):
        self.color = color
        
    def setDistance(self,d):
        self.dist = d

    def setPred(self,p):
        self.pred = p

    def setDiscovery(self,dtime):
        self.disc = dtime
        
    def setFinish(self,ftime):
        self.fin = ftime
        
    def getFinish(self):
        return self.fin
        
    def getDiscovery(self):
        return self.disc
        
    def getPred(self):
        return self.pred
        
    def getDistance(self):
        return self.dist
        
    def getColor(self):
        return self.color
    
    def getConnections(self):
        return self.connectedTo.keys()
        
    def getWeight(self,nbr):
        return self.connectedTo[nbr]
                
    def __str__(self):
        return str(self.id) + ":color " + self.color + ":disc " + str(self.disc) + ":fin " + str(self.fin) + ":dist " + str(self.dist) + ":pred \n\t[" + str(self.pred)+ "]\n"
    
    def getId(self):
        return self.id

class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0

    def addVertex(self,key):
        self.numVertices = self.numVertices + 1
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self,n):
        if n in self.vertList:
            return self.vertList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList

    def addEdge(self,f,t,cost=0):
        if f not in self.vertList:
            nv = self.addVertex(f)
        if t not in self.vertList:
            nv = self.addVertex(t)
        self.vertList[f].addNeighbor(self.vertList[t], cost)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())


def bfs(g,start):
  start.setDistance(0)
  start.setPred(None)
  vertQueue = Queue()
  vertQueue.enqueue(start)
  while (vertQueue.size() > 0):
    currentVert = vertQueue.dequeue()
    for nbr in currentVert.getConnections():
      if (nbr.getColor() == 'white'):
        nbr.setColor('gray')
        nbr.setDistance(currentVert.getDistance() + 1)
        nbr.setPred(currentVert)
        vertQueue.enqueue(nbr)
    currentVert.setColor('black')

def dijkstra(aGraph,start):
    pq = PriorityQueue()
    start.setDistance(0)
    pq.buildHeap([(v.getDistance(),v) for v in aGraph])
    while not pq.isEmpty():
        currentVert = pq.delMin()
        for nextVert in currentVert.getConnections():
            newDist = currentVert.getDistance() \
                    + currentVert.getWeight(nextVert)
            if newDist < nextVert.getDistance():
                nextVert.setDistance( newDist )
                nextVert.setPred(currentVert)
                pq.decreaseKey(nextVert,newDist)
g=Graph()
for i in range(1,200):
  g.addVertex(i)




#dijkstra(g,g.vertList[1])
#d=g.vertList[2].getWeight(g.vertList[3])
#print(d)
#e=g.vertList[3].dist
#print(e)
with open('station.txt') as f: 
      point =[[x if x.isalpha() else int(x) for x in line.split()]for line in f]
          
for i in range(1,469):
      a=(int)(point[i][0])
      b=(int)(point[i][1])
      #print a, b
      g.addEdge(a,b,1)
      g.addEdge(b,a,1)
dijkstra(g,g.vertList[1])
d=[[0 for x in xrange(205)] for x in xrange(205)]
for i in range(1,200):
 for j in range(1,200):
  d[i][j]=g.vertList[j].dist
  #print j, d[1][j]


reachablepoints=[0 for x in xrange(50)]

k=0

def reachablepoint(char,current): 
    i=0                              
    global k
    k=1
    if char!='K':
     while point[i][0]<current:
       if point[i][1]==current:
         if point[i][2]==char:
           reachablepoints[k]=point[i][0]
           k=k+1
       i+=1
     if current!=199:
      while  i<=472  and point[i][0]==current:
       if point[i][2]==char:
         reachablepoints[k]=point[i][1]
         k+=1
       i+=1
    else:
     while point[i][0]<current:
      if point[i][1]==current:
       reachablepoints[k]=point[i][0]
       k+=1
      i+=1
     if current!=199:
      while point[i][0]==current:
       reachablepoints[k]=point[i][1]
       k+=1
       i+=1
"""
class d:
 def __init__(self):
  self.moves=23
  self.bus=8
  self.ug=4
  self.train=10


class x:
 def __init__(self):
  self.moves=23
  self.bus=8
  self.ug=4
  self.train=10
"""


class Node:

 global k
 def __init__(self,depth,dbus,dtrain,dug,xbus,xtrain,xug,pvalue,position,xposition,moves,state=0):
  self.depth=depth
  self.dbus=dbus
  self.dtrain=dtrain
  self.dug=dug
  self.xbus=xbus
  self.xtrain=xtrain
  self.xug=xug
 #self.black=black
  self.pvalue=pvalue
  self.position=position
  self.xposition=xposition
  self.moves=moves
  self.state=self.mindis(position,xposition)
  self.children=[]
  print (self.depth,self.pvalue,position,xposition,self.moves,self.state)
  #if self.depth==0:
  # sys.exit()
  self.createchildren()
  
  
 def mindis(self,p,q):

  return d[p][q]
 
 def createchildren(self):
  global k
  
  if self.pvalue==-1:


   if self.depth>0:
    if self.dbus>0:
     
     reachablepoint('B',self.position)
     reachablepoints1=[0 for x in xrange(50)]
     l1=k
     l1-=1
     while(l1!=0):
      reachablepoints1[l1]=reachablepoints[l1]
      l1-=1
     l1=k
     l1-=1
     while(l1!=0):
       xposition=self.xposition
       position=reachablepoints1[l1]
       
       self.children.append(Node(self.depth-1,self.dbus-1,self.dtrain,self.dug,self.xbus,self.xtrain,self.xug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       
       l1-=1


    if self.dtrain>0:
     reachablepoint('T',self.position)
     reachablepoints2=[0 for x in xrange(50)]
     l2=k
     l2-=1
     while(l2!=0):
      reachablepoints2[l2]=reachablepoints[l2]
      l2-=1
     l2=k
     l2-=1
     while(l2!=0):
       xposition=self.xposition
       position=reachablepoints2[l2]
       print l2, 'fgh'
       self.children.append(Node(self.depth-1,self.dbus,self.dtrain-1,self.dug,self.xbus,self.xtrain,self.xug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       l2-=1
     #if self.depth==2:
        #print l2
      #  sys.exit()
  

    if self.dug>0:
     reachablepoint('U',self.position)
     reachablepoints3=[0 for x in xrange(50)]
     l3=k
     l3-=1
     while(l3!=0):
      reachablepoints3[l3]=reachablepoints[l3]
      l3-=1
     l3=k
     l3-=1
     while(l3!=0):
       xposition=self.xposition
       position=reachablepoints3[l3]
       self.children.append(Node(self.depth-1,self.dbus,self.dtrain,self.dug-1,self.xbus,self.xtrain,self.xug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       l3-=1
  
  else:
   if self.depth>0:


    if self.xbus>0:
     reachablepoint('B',self.xposition)
     reachablepoints4=[0 for x in xrange(50)]
     l4=k
     l4-=1
     while(l4!=0):
      reachablepoints4[l4]=reachablepoints[l4]
      l4-=1
     l4=k
     l4-=1
     while(l4!=0):
       print "hiii",k
       #sys.exit()
       xposition=reachablepoints4[l4]
       position=self.position
       self.children.append(Node(self.depth-1,self.dbus,self.dtrain,self.dug,self.xbus-1,self.xtrain,self.xug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       l4-=1
     


    if self.xtrain>0:
     reachablepoint('T',self.xposition)
     
     print "hi"
     reachablepoints5=[0 for x in xrange(50)]
     l5=k
     l5-=1
     while(l5!=0):
      reachablepoints5[l5]=reachablepoints[l5]
      print reachablepoints[l5]
      l5-=1
     l5=k
     l5-=1
     while(l5!=0):
       #print "ghj",k
       
       position=self.position
       xposition=reachablepoints5[l5]
       #xposition=int(xposition)
       #print(position,xposition)
       self.children.append(Node(self.depth-1,self.dbus,self.dtrain,self.dug,self.xbus,self.xtrain-1,self.xug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       l5-=1
     


    if self.xug>0:
     reachablepoint('U',self.xposition)
     reachablepoints6=[0 for x in xrange(50)]
     l6=k
     l6-=1
     while(l6!=0):
      reachablepoints6[l6]=reachablepoints[l6]
      l6-=1
     l6=k
     l6-=1
     while(l6!=0):
       position=self.position
       xposition=reachablepoints6[l6]
       self.children.append(Node(self.depth-1,self.dbus,self.dtrain,self.dug,self.xbus,self.xtrain,self.xug-1,-self.pvalue,position,xposition,moves-1,self.gamestate(self.moves,position,xposition)))
       k-=1
  
 def gamestate(self,moves,position,xposition):
  if(moves==0 and position!=xposition):
 	  state=-maxsize
  elif(moves>=0 and position==xposition):
          state=maxsize
  else:
          state=self.mindis(position,xposition) 

  return state


def Minmax(node,depth):
   if(depth==0 or moves==0 or xposition==position):
      return node.state

   if node.pvalue==-1:
     bestval=0
     for i in range(len(node.children)):
       child=node.children[i]
       state=Minmax(child,depth-1)
       if(state>bestval):
         bestval=state
     
     return bestval
        
   else:
     bestval=10000
     for i in range(len(node.children)):
       child=node.children[i]
       state=Minmax(child,depth-1)
       if(state<bestval):
         bestval=state
     
     return bestval
 
def Wincheck(moves,position,xposition):
    if((moves>=0 and position==xposition) or (moves<=0 and position!=xposition)):
       print("Game over") 
       return 0
    
    else:
       return 1

if __name__=='__main__':
   moves=23
   dbus=8
   dug=4
   dtrain=10
   xtrain=4
   xbus=3
   xug=3
   pvalue=-1
   depth=1
   curr_position=3
   xposition=10   
   #xposition=int(xposition)

   while(moves>0 and curr_position!=xposition):
     s=input("Where would you like to move next?")
     position=int(s)
     moves=moves-1
     flag=0
     reachablepoint('B',curr_position)
 
     while(k!=0):
         if(reachablepoints[k]==position):
             dbus-=1
             xbus+=1
             flag=1
             break
         k-=1
     
     if(flag==0):
         reachablepoint('T',curr_position)
         while(k!=0):
          if(reachablepoints[k]==position):
            dtrain-=1
            xtrain+=1
            flag=1
            break
          k-=1   
    
     if(flag==0):
         reachablepoint('U',curr_position)
         while(k!=0):
          if(reachablepoints[k]==position):
            dug-=1
            xug+=1
            flag=1
            break
          k-=1    
     
     
     curr_position=position
     best=10000
     if Wincheck(moves,position,xposition):
       #print "hi",train
       #sys.exit()
       pvalue*=-1
       node=Node(depth,dbus,dtrain,dug,xbus,xtrain,xug,pvalue,position,xposition,moves,state=0)
       #sys.exit()
       #print "qwert",train
       best=0
       for i in range(len(node.children)):
        #print "hurray"
        n_child=node.children[i]
         
        print "hurray"
        state=Minmax(n_child,depth-1)
        print state
        state=int(state)
        if(best<state):
          best=state
          l=i
          print best,l
     print d[11][11],  best,l,node.children[l].xposition,"jkjhkjh"
     xposition=node.children[l].xposition
     Wincheck(moves,position,xposition)

     pvalue*=-1




 
  
  
 
