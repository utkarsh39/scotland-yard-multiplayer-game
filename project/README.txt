We have implemented the board game in two versions:	

1) Local Game: This game will be played on a single computer with human pit against computer.

2) LAN Game: This game will be played on LAN involving multiple players 

a) Player v/s AI: Five players (detectives) will connect over LAN to play against the computer (Mr. X).
   
b) Player v/s Player: Five players (detectives) will connect over LAN to play against another player
Mr. X.

The game was coded in Python with GUI being coded using Pygame library.
The AI was implemented using MINIMAX Algorithm and the LAN part was implemented using socket library.

The main code is in dc.py and the minimax is implemented in minimax3.py
