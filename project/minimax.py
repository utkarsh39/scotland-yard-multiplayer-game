from sys import maxsize

import sys
import os
import unittest
class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

class PriorityQueue:
    def __init__(self):
        self.heapArray = [(0,0)]
        self.currentSize = 0

    def buildHeap(self,alist):
        self.currentSize = len(alist)
        self.heapArray = [(0,0)]
        for i in alist:
            self.heapArray.append(i)
        i = len(alist) // 2            
        while (i > 0):
            self.percDown(i)
            i = i - 1
                        
    def percDown(self,i):
        while (i * 2) <= self.currentSize:
            mc = self.minChild(i)
            if self.heapArray[i][0] > self.heapArray[mc][0]:
                tmp = self.heapArray[i]
                self.heapArray[i] = self.heapArray[mc]
                self.heapArray[mc] = tmp
            i = mc
                
    def minChild(self,i):
        if i*2 > self.currentSize:
            return -1
        else:
            if i*2 + 1 > self.currentSize:
                return i*2
            else:
                if self.heapArray[i*2][0] < self.heapArray[i*2+1][0]:
                    return i*2
                else:
                    return i*2+1

    def percUp(self,i):
        while i // 2 > 0:
            if self.heapArray[i][0] < self.heapArray[i//2][0]:
               tmp = self.heapArray[i//2]
               self.heapArray[i//2] = self.heapArray[i]
               self.heapArray[i] = tmp
            i = i//2
 
    def add(self,k):
        self.heapArray.append(k)
        self.currentSize = self.currentSize + 1
        self.percUp(self.currentSize)

    def delMin(self):
        retval = self.heapArray[1][1]
        self.heapArray[1] = self.heapArray[self.currentSize]
        self.currentSize = self.currentSize - 1
        self.heapArray.pop()
        self.percDown(1)
        return retval
        
    def isEmpty(self):
        if self.currentSize == 0:
            return True
        else:
            return False

    def decreaseKey(self,val,amt):
        # this is a little wierd, but we need to find the heap thing to decrease by
        # looking at its value
        done = False
        i = 1
        myKey = 0
        while not done and i <= self.currentSize:
            if self.heapArray[i][1] == val:
                done = True
                myKey = i
            else:
                i = i + 1
        if myKey > 0:
            self.heapArray[myKey] = (amt,self.heapArray[myKey][1])
            self.percUp(myKey)
            
    def __contains__(self,vtx):
        for pair in self.heapArray:
            if pair[1] == vtx:
                return True
        return False
        
class TestBinHeap(unittest.TestCase):
    def setUp(self):
        self.theHeap = PriorityQueue()
        self.theHeap.add((2,'x'))
        self.theHeap.add((3,'y'))
        self.theHeap.add((5,'z'))
        self.theHeap.add((6,'a'))
        self.theHeap.add((4,'d'))


    def testInsert(self):
        assert self.theHeap.currentSize == 5

    def testDelmin(self):
        assert self.theHeap.delMin() == 'x'
        assert self.theHeap.delMin() == 'y'
    
    def testDecKey(self):
        self.theHeap.decreaseKey('d',1)
        assert self.theHeap.delMin() == 'd'
        
#if __name__ == '__main__':
 #   unittest.main()

class Vertex:
    def __init__(self,num):
        self.id = num
        self.connectedTo = {}
        self.color = 'white'
        self.dist = sys.maxsize
        self.pred = None
        self.disc = 0
        self.fin = 0

    # def __lt__(self,o):
    #     return self.id < o.id
    
    def addNeighbor(self,nbr,weight=0):
        self.connectedTo[nbr] = weight
        
    def setColor(self,color):
        self.color = color
        
    def setDistance(self,d):
        self.dist = d

    def setPred(self,p):
        self.pred = p

    def setDiscovery(self,dtime):
        self.disc = dtime
        
    def setFinish(self,ftime):
        self.fin = ftime
        
    def getFinish(self):
        return self.fin
        
    def getDiscovery(self):
        return self.disc
        
    def getPred(self):
        return self.pred
        
    def getDistance(self):
        return self.dist
        
    def getColor(self):
        return self.color
    
    def getConnections(self):
        return self.connectedTo.keys()
        
    def getWeight(self,nbr):
        return self.connectedTo[nbr]
                
    def __str__(self):
        return str(self.id) + ":color " + self.color + ":disc " + str(self.disc) + ":fin " + str(self.fin) + ":dist " + str(self.dist) + ":pred \n\t[" + str(self.pred)+ "]\n"
    
    def getId(self):
        return self.id

class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0

    def addVertex(self,key):
        self.numVertices = self.numVertices + 1
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self,n):
        if n in self.vertList:
            return self.vertList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList

    def addEdge(self,f,t,cost=0):
        if f not in self.vertList:
            nv = self.addVertex(f)
        if t not in self.vertList:
            nv = self.addVertex(t)
        self.vertList[f].addNeighbor(self.vertList[t], cost)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())


def bfs(g,start):
  start.setDistance(0)
  start.setPred(None)
  vertQueue = Queue()
  vertQueue.enqueue(start)
  while (vertQueue.size() > 0):
    currentVert = vertQueue.dequeue()
    for nbr in currentVert.getConnections():
      if (nbr.getColor() == 'white'):
        nbr.setColor('gray')
        nbr.setDistance(currentVert.getDistance() + 1)
        nbr.setPred(currentVert)
        vertQueue.enqueue(nbr)
    currentVert.setColor('black')

def dijkstra(aGraph,start):
    pq = PriorityQueue()
    start.setDistance(0)
    pq.buildHeap([(v.getDistance(),v) for v in aGraph])
    while not pq.isEmpty():
        currentVert = pq.delMin()
        for nextVert in currentVert.getConnections():
            newDist = currentVert.getDistance() \
                    + currentVert.getWeight(nextVert)
            if newDist < nextVert.getDistance():
                nextVert.setDistance( newDist )
                nextVert.setPred(currentVert)
                pq.decreaseKey(nextVert,newDist)
g=Graph()
for i in range(1,200):
  g.addVertex(i)




#dijkstra(g,g.vertList[1])
#d=g.vertList[2].getWeight(g.vertList[3])
#print(d)
#e=g.vertList[3].dist
#print(e)
with open('station.txt') as f: 
      point =[[x if x.isalpha() else int(x) for x in line.split()]for line in f]
          
for i in range(1,469):
      a=(int)(point[i][0])
      b=(int)(point[i][1])
      #print a, b
      g.addEdge(a,b,1)
      g.addEdge(b,a,1)
dijkstra(g,g.vertList[1])
d=[[0 for x in xrange(205)] for x in xrange(205)]
for i in range(1,200):
 for j in range(1,200):
  d[i][j]=g.vertList[j].dist
  #print j, d[1][j]



reachablepoints=[0 for x in xrange(50)]

k=0

def reachablepoint(char,current): 
    i=0                              
    global k
    k=1
    if char!='K':
     while point[i][0]<current:
       if point[i][1]==current:
         if point[i][2]==char:
           reachablepoints[k]=point[i][0]
           k=k+1
       i+=1
     if current!=199:
      while  i<=472  and point[i][0]==current:
       if point[i][2]==char:
         reachablepoints[k]=point[i][1]
         k+=1
       i+=1
    else:
     while point[i][0]<current:
      if point[i][1]==current:
       reachablepoints[k]=point[i][0]
       k+=1
      i+=1
     if current!=199:
      while point[i][0]==current:
       reachablepoints[k]=point[i][1]
       k+=1
       i+=1

class d:
 def __init__(self):
  self.moves=23
  self.bus=8
  self.ug=4
  self.train=10


class x:
 def __init__(self):
  self.moves=23
  self.bus=8
  self.ug=4
  self.train=10



class Node:

 global k
 def __init__(self,depth,bus,train,ug,pvalue,position,xposition,moves,state=0):
  self.depth=depth
  self.bus=bus
  self.train=train
  self.ug=ug
 #self.black=black
  self.pvalue=pvalue
  self.position=position
  self.xposition=xposition
  self.moves=moves
  self.children=[]
  #print(self.depth,self.bus,self.train,self.ug,-self.pvalue,position,xposition,self.moves)
  #sys.exit()
  self.createchildren()
  self.state=self.mindis(position,xposition)
  
 def mindis(self,p,q):

  return d[p][q]
 
 def createchildren(self):
  global k
  
  if self.pvalue==-1:
   if self.depth>0:
    if self.bus>0:
     
     reachablepoint('B',self.position)
     k-=1
     while(k!=0):
       xposition=self.xposition
       position=reachablepoints[k]
       
       self.children.append(Node(self.depth-1,self.bus-1,self.train,self.ug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       
       k-=1
    if self.train>0:
     reachablepoint('T',self.position)
     k-=1
     while(k!=0):
       xposition=self.xposition
       position=reachablepoints[k]
       
       self.children.append(Node(self.depth-1,self.bus,self.train-1,self.ug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       
       k-=1
    if self.ug>0:
     reachablepoint('U',self.position)
     k-=1
     while(k!=0):
       xposition=self.xposition
       position=reachablepoints[k]
       self.children.append(Node(self.depth-1,self.bus,self.train,self.ug-1,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       k-=1
  
  else:
   if self.depth>0:
    if self.bus>0:
     reachablepoint('B',self.xposition)
     k-=1
     while(k!=0):
       #print "hiii",k
       #sys.exit()
       xposition=reachablepoints[k]
       position=self.position
       self.children.append(Node(self.depth-1,self.bus-1,self.train,self.ug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       k-=1
    if self.train>0:
     reachablepoint('T',self.xposition)
     #print self.xposition
     k-=1
     while(k!=0):
      # print "ghj",k
       
       position=self.position
       xposition=reachablepoints[k]
       #xposition=int(xposition)
      # print(position,xposition)
       self.children.append(Node(self.depth-1,self.bus,self.train-1,self.ug,-self.pvalue,position,xposition,self.moves-1,self.gamestate(self.moves,position,xposition)))
       k-=1
    if self.ug>0:
     reachablepoint('U',self.xposition)
     k-=1
     while(k!=0):
       position=self.position
       xposition=reachablepoints[k]
       self.children.append(Node(self.depth-1,self.bus,self.train,self.ug-1,-self.pvalue,position,xposition,moves-1,self.gamestate(self.moves,position,xposition)))
       k-=1
  
 def gamestate(self,moves,position,xposition):
  if(moves==0 and position!=xposition):
 	  state=-maxsize
  elif(moves>=0 and position==xposition):
          state=maxsize
  else:
          state=self.mindis(position,xposition) 

  return state


def Minmax(node,depth):
   if(depth==0 or moves==0 or xposition==position):
      return node.state

   if node.pvalue==-1:
     bestval=0
     for child in node.children():
       child=node.children[i]
       state=Minmax(child,depth-1)
       if(state>bestval):
         bestval=state
     
     return bestval
        
   else:
     bestval=10000
     for child in node.children():
       child=node.children[i]
       state=Minmax(child,depth-1)
       if(state<bestval):
         bestval=state
     
     return bestval
 
def Wincheck(moves,position,xposition):
    if((moves>=0 and position==xposition) or (moves<=0 and position!=xposition)):
       print("Game over") 
       return 0
    
    else:
       return 1

if __name__=='__main__':
  
   pvalue=-1
   depth=1
   curr_position=1
   xposition=10   
   #xposition=int(xposition)
   d1=d()
   x=x()
   while(moves>0 and curr_position!=xposition):
     s=input("Where would you like to move next?")
     position=int(s)
     d1.moves=d1.moves-1
     flag=0
     reachablepoint('B',curr_position)
 
     while(k!=0):
         if(reachablepoints[k]==position):
             d1.bus-=1
             x.bus+=1
             flag=1
             break
         k-=1
     
     if(flag==0):
         reachablepoint('T',curr_position)
         while(k!=0):
          if(reachablepoints[k]==position):
            d1.train-=1
            x.train+=1
            flag=1
            break
          k-=1   
    
     if(flag==0):
         reachablepoint('U',curr_position)
         while(k!=0):
          if(reachablepoints[k]==position):
            d1.ug-=1
            x.ug+=1
            flag=1
            break
          k-=1    
     
     
     curr_position=position
     best=10000
     if Wincheck(x.moves,position,xposition):
       #print "hi",train
       #sys.exit()
       pvalue*=-1
       node=Node(depth,x.bus,x.train,x.ug,pvalue,position,xposition,x.moves,state=0)
       #print "qwert",train
       
       for i in range(len(node.children)):
        #print "hurray"
        n_child=node.children[i]
        best=0 
        state=Minmax(n_child,depth-1)
        if(best<state):
          best=state
          l=i
     print best,l,"hjkjhkjh"
     
     Wincheck(moves,position,xposition)

     pvalue*=-1




 
  
  
 
