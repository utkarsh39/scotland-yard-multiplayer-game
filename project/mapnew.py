import pygame, Buttons,os,sys,random,subprocess,tkMessageBox
from pygame.locals import *
from array import *
from Tkinter import *
window = Tk()
window.wm_withdraw()
progname = sys.argv[0]
progdir = os.path.dirname(progname)
sys.path.append(os.path.join(progdir,'gamelib'))
from popup_menu import PopupMenu
FPS=20
FPSCLOCK = pygame.time.Clock() 
pygame.init()
myfont = pygame.font.SysFont("monospace", 15)

k=0
turn=0 #index of whose turn
b=0 #y cordinate of mouse
img_posx=0
img_posy=0
cardch=0 # index of card
WON=0 #won=1 detective won=2 mrx
reachablepoints=[int]*50
startpoint=(13,26,29,34,50,53,91,94,103,112,117,132,138,141,155,174,197,198)

with open('SCOTPOS.TXT') as f:  #contains info of cordinates
    position =[[int(x) for x in line.split()]for line in f]


with open('SCOTMAP.TXT') as f:    #contains info of where u can go using which ticket
    w, h = [int(x) for x in f.readline().split()]
    point =[[x if x.isalpha() else int(x) for x in line.split()]for line in f]


#for start points
s=random.sample(xrange(18), 6)

d1=startpoint[s[0]]
d2=startpoint[s[1]]
d3=startpoint[s[2]]
d4=startpoint[s[3]]
d5=startpoint[s[4]]
x1=startpoint[s[5]]

count=0
i=1
while count<=5:#
 if d1==position[i][0]:
  d1=position[i]
  count+=1
  i+=1
 elif d2==position[i][0]:
  d2=position[i]
  count+=1
  i+=1
 elif d3==position[i][0]:
  d3=position[i]
  count+=1
  i+=1
 elif d4==position[i][0]:
  d4=position[i]
  count+=1
  i+=1
 elif d5==position[i][0]:
  d5=position[i]
  count+=1
  i+=1
 elif x1==position[i][0]:
  x1=position[i]
  count+=1
  i+=1
 else:
  i+=1


def caught(a,b):
 global WON

 if a==b:
  WON=1
 
#handle the popup menu input
def handle_menu(e):
     # print 'Menu event: %s %s' % (e.name,e.text)
    if(e.text!='NOT_POSSIBLE'):
       return int(e.text)
    else:
       tkMessageBox.showinfo(title="ERROR", message="Please Select a Possible Move")
       return 0

def searchcordi(point):
     
       i=0
       while point!=position[i][0]:
         i+=1
       return position[i]



class detective:
   movable=1 # 0=not able to move
   global d1,d2,d3,d4,d5,x1,b,char
   T=10
   B=8  
   U=4


   def __init__(self):
      self.b=b
      self.main()
        

   def card(self,current):
    global cardch
    if 0<b<60:
     cardch='B'
     return 'B'
    elif 60<b<120:
     cardch='T'
     return 'T'
    elif 120<b<180:
     cardch='U'
     return 'U'

  

   #reachable POINTS             
   def reachablepoint(self,char,current): 
    i=0                              
    global k
    k=1
    if char!='K':
     while point[i][0]<current:
       if point[i][1]==current:
         if point[i][2]==char:
           reachablepoints[k]=point[i][0]
           k=k+1
       i+=1
     if current!=199:
      while  i<=472  and point[i][0]==current:
       if point[i][2]==char:
         reachablepoints[k]=point[i][1]
         k+=1
       i+=1
    else:
     while point[i][0]<current:
      if point[i][1]==current:
       reachablepoints[k]=point[i][0]
       k+=1
      i+=1
     if current!=199:
      while point[i][0]==current:
       reachablepoints[k]=point[i][1]
       k+=1
       i+=1

    while k<2:
     reachablepoints[k]='NOT_POSSIBLE'
     k+=1
   

    menu=()
    for j in range(0,k):
      menu=menu+tuple((str(reachablepoints[j])).split(" "),)
    (a,b)=pygame.mouse.get_pos()
    if 0<b<180 and 1218<a<1364:
      PopupMenu(menu)




   def main(self):
        global turn
        
        if turn<(22*6):
            if turn%6==0 and mrx.movable==1:
              mrx()
            elif turn%6==1 and detective1.movable==1:
              
               detective1()
            elif turn%6==2 and detective2.movable==1:
             
               detective2()
            elif turn%6==3 and detective3.movable==1:
              
               detective3()
            elif turn%6==4 and detective4.movable==1:
              
              detective4()
            elif turn%6==5 and detective5.movable==1:
              
               detective5()
            else:
               turn+=1
            

        else:
          WON=2

class detective1(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d1[0]
     char=self.card(current)
     if getattr(detective1,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective1,char))
        self.reachablepoint(char,current)
        display()
    
class detective2(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d2[0]
     char=self.card(current)
     if getattr(detective2,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective2,char))
        self.reachablepoint(char,current)
        display()
     
class detective3(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()
   

  def main(self):
     current=d3[0]
     char=self.card(current)
     if getattr(detective3,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective3,char))
        self.reachablepoint(char,current)
        display()
    
class detective4(detective):
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=d4[0]
     char=self.card(current)
     if getattr(detective4,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective4,char))
        self.reachablepoint(char,current)
        display()

class detective5(detective):
  def __init__(self):
    #super(detective1, self).__init__(self)
    detective.__init__(self)
    self.main()

  def main(self):
     current=d5[0]
     char=self.card(current)
     if getattr(detective5,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(detective5,char))
        self.reachablepoint(char,current)
        display()



class mrx(detective):
  T=4
  B=3 
  U=3
  X=2
  K=5
  def __init__(self):
    detective.__init__(self)
    self.main()

  def main(self):
     current=x1[0]
     char=self.card(current)
     if getattr(mrx,char)>0:
        reachablepoints[0]='Cards_Left=%s'%(getattr(mrx,char))
        self.reachablepoint(char,current) 
        display()
       

class Button:
  def __init__(self):
        self.main()
     
  def update_display(self):
        #Parameters:       s        surface,      color,       x,   y,   length, height, width,    text,      text_color
        self.Button1.create_button(DISPLAYSURF, (255,255,255), 1040, 650, 110,    100,    0,       " BACK", (0,0,0))
        pygame.display.flip()
    
  def main(self):
        self.Button1 = Buttons.Button()
        self.update_display()

   
                    
                     
if True: 
        
        
        def display(): 
          
          DISPLAYSURF.blit(mapImg, (0, 0),pygame.Rect(img_posx,img_posy,1033,710))
          DISPLAYSURF.blit(taxi, (1218, 60))
          DISPLAYSURF.blit(bus, (1218, 0))
          DISPLAYSURF.blit(underground, (1218, 120))
          mapImg.blit(img0, (2*x1[1]-20,2*x1[2]-40))
          mapImg.blit(img1, (2*d1[1]-20,2*d1[2]-40))
          mapImg.blit(img2, (2*d2[1]-20,2*d2[2]-40))
          mapImg.blit(img3, (2*d3[1]-20,2*d3[2]-40))
          mapImg.blit(img4, (2*d4[1]-20,2*d4[2]-40))
          mapImg.blit(img5, (2*d5[1]-20,2*d5[2]-40))
          label = myfont.render("Some text!", 1, (255,255,0))
          DISPLAYSURF.blit(label, (1033, 180))
          pygame.display.update()
 
        def scroll(mousex,mousey):
          global img_posx
          global img_posy
          if 1033>mousex>850 and img_posx<1020:
             img_posx+=20
          elif mousex<120 and img_posx>0:
             img_posx-=20
          elif mousey>660 and img_posy<900 and mousex<1033:
             img_posy+=20
          elif mousey<120 and img_posy>0 and mousex<1033:
             img_posy-=20
          #display()######
       
      
        DISPLAYSURF = pygame.display.set_mode((1364,710 ))
        mapImg=pygame.image.load('map.jpg')
        mapImg=pygame.transform.scale(mapImg, (2036, 1618))
        #DISPLAYSURF.blit(mapImg, (0, 0),pygame.Rect(img_posx,img_posy,1033,710))
        pygame.display.set_caption('Scotland Yard')
        img0=pygame.image.load('flag0.gif')
        img0=pygame.transform.scale(img0, (50, 50))
        img1=pygame.image.load('flag1.gif')
        img1=pygame.transform.scale(img1, (50, 50))
        #mapImg.blit(pawnImg1, (900,0))
        img2=pygame.image.load('flag2.gif')
        img2=pygame.transform.scale(img2, (50, 50))
        img3=pygame.image.load('flag3.gif')
        img3=pygame.transform.scale(img3, (50, 50))
        img4=pygame.image.load('flag4.gif')
        img4=pygame.transform.scale(img4, (50, 50))
        img5=pygame.image.load('flag5.gif')
        img5=pygame.transform.scale(img5, (50, 50))
        bus=pygame.image.load('bus.png')
        bus=pygame.transform.scale(bus, (146, 60))
        DISPLAYSURF.blit(bus, (1218, 0))
        taxi=pygame.image.load('taxi.png')
        taxi=pygame.transform.scale(taxi, (146, 60))
        DISPLAYSURF.blit(taxi, (1218, 60))
        underground=pygame.image.load('underground.png')
        underground=pygame.transform.scale(underground, (146, 60))
        DISPLAYSURF.blit(underground, (1218, 120))
        badge=pygame.image.load('dbadge.jpg')
        badge=pygame.transform.scale(badge, (200, 180))
        DISPLAYSURF.blit(badge, (1033, 0), (0,0, 185, 180))
        Button()
        pygame.display.update()

        display()
        while WON==0:
         
         
         if detective1.movable==0 and detective2.movable==0 and detective3.movable==0 and detective4.movable==0 and detective5.movable==0:
          WON=2
         for event in pygame.event.get():
                (a,b)=pygame.mouse.get_pos()
                scroll(a,b)
                if event.type == pygame.QUIT:
                   pygame.quit()

                elif event.type == USEREVENT:
                   
                   if event.code == 'MENU':
                        
                        if turn<(22*6) and handle_menu(event)!=0:
                                       if turn%6==0 :
                                        if cardch=='T':
                                         mrx.T-=1
                                        elif cardch=='B':
                                         mrx.B-=1
                                        elif cardch=='K':
                                         mrx.K-=1
                                        elif cardch=='X':
                                         mrx.X-=1
                                        elif cardch=='U':
                                         mrx.U-=1
                                        x1=searchcordi(handle_menu(event))  
                                       elif turn%6==1 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective1.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective1.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective1.U-=1
                                        d1=searchcordi(handle_menu(event))
                                       elif turn%6==2 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective2.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective2.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective2.U-=1
                                        d2=searchcordi(handle_menu(event))
                                       elif turn%6==3 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective3.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective3.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective3.U-=1
                                        d3=searchcordi(handle_menu(event))
                                       elif turn%6==4 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective4.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective4.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective4.U-=1
                                        d4=searchcordi(handle_menu(event))
                                       elif turn%6==5 :
                                        if cardch=='B':
                                         mrx.B+=1
                                         detective5.B-=1
                                        elif cardch=='T':
                                         mrx.T+=1
                                         detective5.T-=1
                                        elif cardch=='U':
                                         mrx.U+=1
                                         detective5.U-=1
                                        d5=searchcordi(handle_menu(event))
                                       if turn%6!=0 :
                                        caught(x1[0],handle_menu(event)) 
                                       mapImg=pygame.image.load('map.jpg')
                                       mapImg=pygame.transform.scale(mapImg, (2036, 1618))
                                       display()
                                       turn+=1

                elif event.type == MOUSEBUTTONDOWN:
                   #if Buttons.Button.pressed(pygame.mouse.get_pos()):
                       #import start
                  
                   (a,b)=pygame.mouse.get_pos()
                   if 0<b<180 and 1218<a<1364:
                      detective()
                      
                 
                display()
                FPSCLOCK.tick(FPS)
                


        while WON==1: #detective won
           dwon=pygame.image.load('dwon.jpg')
           #dwon=pygame.transform.scale(dwon, (146, 60))
           DISPLAYSURF.blit(dwon, (0,0))
           pygame.display.update()
         

        while WON==2: # xwon
             xwon=pygame.image.load('xwon.jpg')
             #xwon=pygame.transform.scale(underground, (146, 60))
             DISPLAYSURF.blit(xwon, (0,0))