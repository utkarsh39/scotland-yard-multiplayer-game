# -*- coding: cp1252 -*-
#/usr/bin/env python
#Simon H. Larsen
#Buttons.py - example
#Project startet: d. 28. august 2012
#Import pygame modules and Buttons.py(it must be in same dir)
import os
import sys
import pygame, Buttons
from pygame.locals import *

#Initialize pygame
pygame.init()
x1=0
y1=0
x2=10
y2=10
x3=20
y3=20
x4=30
y4=30
x5=40
y5=40
progname = sys.argv[0]
progdir = os.path.dirname(progname)
sys.path.append(os.path.join(progdir,'gamelib'))
from popup_menu import PopupMenu
class Button_Example:
    def __init__(self):
        self.main()
    x=0
    y=0
    #Create a display
    def display(self,a,b,c,d,e,f,g,h,i,j):
        global x1
        global y1
        global x2
        global y2
        global x3
        global y3
        global x4
        global y4
        global x5
        global y5
        self.screen = pygame.display.set_mode((1800,770))
        #Displaysurf = self.screen.convert_alpha()
        pygame.display.set_caption("Scotland Yard!")
        #DISPLAYSURF = pygame.display.set_mode((1025, 770))
        mapImg=pygame.image.load('map.jpg')
        mapImg=pygame.transform.scale(mapImg, (1018, 750))
        self.screen.blit(mapImg, (0, 0))       
        #self.screen.blit(Displaysurf,(0,0))
        pawnImg1=pygame.image.load('grey.jpg')
        pawnImg1=pygame.transform.scale(pawnImg1, (15, 35))
        self.screen.blit(pawnImg1, (a,b))
        pawnImg2=pygame.image.load('blue.jpg')
        pawnImg2=pygame.transform.scale(pawnImg2, (15, 35))
        self.screen.blit(pawnImg2, (c,d))
        pawnImg3=pygame.image.load('green.jpg')
        pawnImg3=pygame.transform.scale(pawnImg3, (15, 35))
        self.screen.blit(pawnImg3, (e,f))
        pawnImg4=pygame.image.load('purple.jpg')
        pawnImg4=pygame.transform.scale(pawnImg4, (15, 35))
        self.screen.blit(pawnImg4, (g,h))
        pawnImg5=pygame.image.load('red.jpg')
        pawnImg5=pygame.transform.scale(pawnImg5, (15, 35))
        self.screen.blit(pawnImg5, (i,j))

    #Update the display and show the button
    def update_display(self):
        global x1
        global y1
        global x2
        global y2
        global x3
        global y3
        global x4
        global y4
        global x5
        global y5
        
        #self.screen.fill((30,144,255))
        #Parameters:               surface,      color,       x,   y,   length, height, width,    text,      text_color
        self.Button1.create_button(self.screen, (255,0,0), 1023, 680, 200,    100,    0,        "BACK", (255,255,255))
        pygame.display.flip()
    
    
    def handle_menu(e):
      print 'Menu event: %s.%d: %s' % (e.name,e.item_id,e.text)
      if e.name == 'Main':
          if e.text == 'Quit':
              quit()
      elif e.name == 'Things':
          pass
      elif e.name == 'More Things':
          pass

    #Run the loop
    def main(self):
        global x1
        global y1
        global x2
        global y2
        global x3
        global y3
        global x4
        global y4
        global x5
        global y5
        self.Button1 = Buttons.Button()
        self.display(x1,y1,x2,y2,x3,y3,x4,y4,x5,y5)
        menu_data = (
           'Main',
           'Item 0',
           'Item 1',
           (
              'Things',
              'Item 0',
              'Item 1',
              'Item 2',
              (
                'More Things',
                'Item 0',
                'Item 1',
              ),
           ),
           'Quit',
        )
        while True:
             self.update_display()
             pygame.display.flip()
             for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == MOUSEBUTTONUP:
                 x1=189
                 y1=42
                 x2=289
                 y2=42
                 x3=389
                 y3=42
                 x4=489
                 y4=42
                 x5=589
                 y5=42
                 self.display(x1,y1,x2,y2,x3,y3,x4,y4,x5,y5)
                 self.update_display()
                 PopupMenu(menu_data)
                elif event.type == USEREVENT:
                   if event.code == 'MENU':
                     handle_menu(event)
                elif event.type == MOUSEBUTTONDOWN:
                    print pygame.mouse.get_pos()
                    if self.Button1.pressed(pygame.mouse.get_pos()):
                        #print "Give me a command!"
                        import start
                        testmap.exit()
if True:                            
#if __name__ == '__main__':
    obj = Button_Example()
